package pageobjects.aufa;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class FindMyDevicePO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public FindMyDevicePO(AppiumDriver<WebElement> driver){
        this.driver = driver;
        appium = new AppiumHelpers(driver);

        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    @AndroidFindBy(id = "sign_in_button")
    private WebElement buttonContinueAs;

    @AndroidFindBy(id = "account")
    private WebElement account;

    @AndroidFindBy(id = "password_text")
    private WebElement passwordEditText;

    @AndroidFindBy(id = "sign_in_button")
    private WebElement buttonSignIn;

    @AndroidFindBy(id = "device_panel_info")
    private WebElement verifyLogin;

    @AndroidFindBy(id = "btn_ring")
    private WebElement playSoundButton;

    @AndroidFindBy(id = "btn_stop_ring")
    private WebElement stopRingButton;

    @AndroidFindBy(id = "ring_text_section_1")
    private WebElement verifyPlaySound;


    /**
     * Click on Button Continue As
     */
    public void clickOnButtonContinnueAs() throws InterruptedException {
        buttonContinueAs.click();
    }

    /**
     * Click on Account
     */
    public void clickOnAccount() throws InterruptedException {
        account.click();
    }

    /**
     * Input password
     * @param Password
     */
    public void enterPassword(String Password) throws InterruptedException {
        appium.hardWait(5);
        appium.enterText(passwordEditText, Password, true);
    }

    /**
     * Click on Button Sign In
     */
    public void clickOnButtonSignIn() throws InterruptedException {
        buttonSignIn.click();
    }

    /**
     * Verify Login Success
     */
    public void verifyLoginSuccess() throws InterruptedException{
        appium.isElementEnabled(verifyLogin);
    }

    /**
     * Click on Play Sound Button
     */
    public void clickOnPlaySoundButton() throws InterruptedException {
        playSoundButton.click();
    }

    /**
     * Click on Stop Ring Button
     */
    public void clickOnStopRingButton() throws InterruptedException {
        stopRingButton.click();
    }

    /**
     * Verify Play Sound Success
     */
    public void verifyPlaySoundSuccess() throws InterruptedException{
        appium.isElementEnabled(verifyPlaySound);
    }

}



