package steps.aufa;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageobjects.aufa.FindMyDevicePO;
import utilities.ThreadManager;

public class FindMyDeviceSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private FindMyDevicePO findDevice = new FindMyDevicePO(driver);

    @Given("user click button continue as")
    public void user_click_button_continue_as() throws InterruptedException {
        findDevice.clickOnButtonContinnueAs();
    }

    @When("user click account")
    public void user_click_account() throws InterruptedException {
        findDevice.clickOnAccount();
    }

    @When("user input password {string}")
    public void user_input_password(String Password) throws InterruptedException {
        findDevice.enterPassword(Password);
    }

    @When("user click button sign in")
    public void user_click_button_sign_in() throws InterruptedException {
        findDevice.clickOnButtonSignIn();
    }

    @When("user click button play sound")
    public void user_click_button_play_sound() throws InterruptedException {
        findDevice.clickOnPlaySoundButton();
    }

    @When("user click button stop ring")
    public void user_click_button_stop_ring() throws InterruptedException {
        findDevice.clickOnStopRingButton();
    }

    @Then("system display login success")
    public void system_display_login_success() throws InterruptedException {
        findDevice.verifyLoginSuccess();
    }

    @Then("system display play sound success")
    public void system_display_play_sound_success() throws InterruptedException {
        findDevice.verifyPlaySoundSuccess();
    }

}

