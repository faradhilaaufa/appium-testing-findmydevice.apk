package runners;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/findmydevice/cucumber-report.json",  "html:target/results/findmydevice"},
        features = "src/test/resources/features/aufa",
        glue = "steps",
        tags = {"@findmydevice"}
)

public class FindMyDeviceTestRunner extends BaseTestRunnerAndroid {

}

